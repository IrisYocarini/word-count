.. Documentation documentation master file, created by
   sphinx-quickstart on Wed Feb 17 11:27:32 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Documentation's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   changes


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
